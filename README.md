---
The VIM Toolset Installer
---

The VIM Toolset is a collection of my favorite plugins, themes, and language
detection routines for VIM. I put it together to make moving between machines
quicker and to share my configuration with friends and the internet.

I provide no warranty; every plugin should include the original repository and
thus the original licensure for the plugin per requirements. If you are the
original developer or current maintainer and you notice anything missing from
the required FOSS licensure documentation please let me know and I will be more
than happy to fix the issue.

Refer to the INSTALL document for more about how to install the toolkit as well
as some good advice for avoiding mishaps or unexpected outcomes.

--Kellin
